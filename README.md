# OvirtTools


Build a wrapper tools around ovirtSDK python tools to made easy access from script or bash to the RHEV manager function.

The project is in the beta phase and have the main interface to access to the VMs manager capability. The commands are designed to be executed as regular commands in a bash script.

## Getting started

- Install ovirtsdk4. Full info may be found [here](https://github.com/oVirt/python-ovirt-engine-sdk4)
- download ovirtTools.
- configure it
- use it!


## How to configure *ovirtTools*

There are two steps:
- the first, easy, is to recover the CA of the manager API. If self-signed, it may be recovered by the following command:
```
wget --output-document ca.crt 'http:///manager.domain.tpl/ovirt-engine/services/pki-resource?resource=ca-certificate&format=X509-PEM-CA'
```
- the second think is to configure the *cfg.ini* configuration file to permit to all script to manage easily handle operations on manage

##### cfg.ini configration file

```
[DEFAULT]
manager_cfg_path = cfg.manager
default_manager = <label of default manager - like "the_first">

[the_first]
ARG = parameter_to_use_with_script_to_reference_manager
TITLE = _TITLE_USED_IN_HELP
URL = https://manager.domain.tpl/ovirt-engine/api
USER = admin@internal
PWD = _the_pwd_of_USER
CA_CRT_FILE = file_name_of_CA_under_manager_cfg_path
EXPORT_DOMAIN = EXPORT-DMAIN_LABEL
```

#### Describe section:
The **Default** section is the description of ... default. So, no other words are to update
The **the_first** section is the description used by script to access to the manager feature / capability. If more manager we want to reach, more section we need to add to the cfg.ini file



## How to user *ovirtTools*
### manage host: *manager*
```
[code ]$ manager -h
usage: manager [-h] [--dwn] [--stam] [--meat] [--sdat] [-q] [-c] [--host HOST] [--vm VM] command

RHEV manager interface

positional arguments:
  command      may be one of:
                <listmgr> for listing manager available
                <hostlist> for listing hosts available
                <hostinfo> for show hosts info
                <getvmlist> for listing VM (on desidered node if --host is used)
                <getstats> for retrieve full statistical infos of --host or --vm objec

options:
  -h, --help   show this help message and exit
  --manager1            use TITLE_USED_IN_HELP_1 manager to operate
  --manager2            use TITLE_USED_IN_HELP_2 manager to operate
  --manager3            use TITLE_USED_IN_HELP_3 manager to operate
  -q           Suppress print of label; usefull to pipeling command
  -c
  --host HOST  used to define operational host
  --vm VM      used to define operational VM
```

### manage virtual manchine: *VMmanager*
```
[code ]$ VMmanager -h
usage: VMmanager [-h] [--manager1] [--manager2] [--manager3] [--host destination_host] VM command

RHEV manager interface

positional arguments:
  VM                    VM to execute the command
  command               may be one of:
                                status  get info of VM
                                start
                                shutdown
                                migrate --host is needs to specify host
                                info
                                fullinfo
                                list (accept wildcard "*"); if --host is used, search only in the selected host
                                disklist (accept wildcard "*")  backup

options:
  -h, --help            show this help message and exit
  --manager1            use TITLE_USED_IN_HELP_1 manager to operate
  --manager2            use TITLE_USED_IN_HELP_2 manager to operate
  --manager3            use TITLE_USED_IN_HELP_3 manager to operate
  --host destination_host
```
description... soon


## License
For open source projects, 

