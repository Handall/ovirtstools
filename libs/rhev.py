import time
import sys
from enum import Enum

import ovirtsdk4 as sdk
import ovirtsdk4.types as types

import libs.datas as DATAS
import xml.etree.ElementTree as ET


connection  = ""
sys_service = ""
dcs_service = ""
dc_service = ""
vms_service = ""
sds_service = ""



### --- Types



class ObjType(Enum):
    HOST    = 1
    VM      = 2

class infoMode(Enum):
    MINIMAL = 0x00
    BASE    = 0x02
    CPU     = 0x10
    RAM     = 0x20
    PIPPO   = 0x60
    FULL    = 0xFF

class printMode(Enum):
    get         = 0x0
    csv         = 0x1
    formatted   = 0xF

class statsMode(Enum):
    cpu         = 0x001
    memory      = 0x002
    cpumem      = 0x003
    swap        = 0x004
    disks       = 0x008
    network     = 0x010
    hugepages   = 0x080
    migration   = 0x100
    boot        = 0x200
    ksm         = 0x400
    elapsed     = 0x800
    hostLoad    = 0xFFF
    full        = 0xFFF

### --- Functions



def getConnection(URL, USR, PWD, CA):
    #
    # Create a connection to the server:
    global connection, script_dir
    connection = sdk.Connection(
      url       = URL,
      username  = USR,
      password  = PWD,
      ca_file   = CA,
    )



def closeConnection():
    global connection
    connection.close()



def getDCSService():
    global connection
    global sys_service
    global dcs_service

    if dcs_service == "":
        if sys_service == "":
            sys_service = connection.system_service()

        dcs_service = sys_service.data_centers_service()

    return dcs_service



def getDCService(name):
    global connection
    global dcs_service
    global dc_service

    if dc_service == "":
        if dcs_service == "":
            dcs_service = getDCSService()
        if name == "":
            raise ValueError('Data Center name not defined')

        dc = dcs_service.list(search='name=' + name)[0]
        dc_service = dcs_service.data_center_service(dc.id)

    return dc_service



def getVMSService():
    global connection
    global sys_service
    global vms_service

    if vms_service == "":
        if sys_service == "":
            sys_service = connection.system_service()

        vms_service = sys_service.vms_service()

    return vms_service



def getSDSService():
    global connection
    global sys_service
    global sds_service

    if sds_service == "":
        if sys_service == "":
            sys_service = connection.system_service()

        sds_service = sys_service.storage_domains_service()

    return sds_service



#############################################################################
####                                                                     ####
####     Manager functions                                               ####
####                                                                     ####
#############################################################################



def RHV_getInfo(target, mode=infoMode.BASE, prn=printMode.csv):
    global connection
    host_service = connection.system_service().hosts_service()

    hosts = host_service.list()

    if prn == printMode.csv:
        print("\n%015s  %-22s  %-13s %-5s %-5s %-5s %-5s" % ("CLUSTER", "HOST", "STATUS", "SOCKS", "CORES", "THDS", "RAM"))
    for host in hosts:
        cluster=connection.follow_link(host.cluster)
        if prn == printMode.csv:
            print("%015s  %-22s  %-13s  %-5s %-5s %-4s %-.0f GB" % (cluster.name, host.name.split(".")[0], host.status.name,  host.cpu.topology.sockets, host.cpu.topology.cores, host.cpu.topology.threads, (host.memory / 1024 /1024/ 1024) ))
        elif prn == printMode.formatted:
            print("\n" + host.name)
            if host.hosted_engine:
                print("  %-15s %s" % ("type:", "* HOSTED ENGINE"))
            print("  %-15s %s" % ("status:", host.status.name))
            if host.status_detail:
                print("  %-15s %s" % ("status:", host.status_detail))
            if host.spm.status != types.SpmStatus.NONE:
                print("  %-15s %s" % ("rule:", host.spm.status.name))
            #print("\t" + host.spm.priority)
            print("  %-15s %s"          % ("cpu family:", host.cpu.type))
            print("  %-15s %s"          % ("cpu type:", host.cpu.name))
            #print(" %-15s %s"          % ("cpu type:", host.cpu.speed))
            print("  %-15s %s"          % ("cpu socket:", host.cpu.topology.sockets))
            print("  %-15s %s"          % ("cpu cores:", host.cpu.topology.cores))
            print("  %-15s %s"          % ("cpu threads:", host.cpu.topology.threads))
            print("  %-15s %d MB"       % ("memory:",(host.memory / 1024 /1024)))
            print("  %-15s %s"          % ("vdsm ver:", host.version.full_version))
            print("  %-15s %s"          % ("ovn configured", host.ovn_configured))
            #???? print(host.power_management.status)
        #print("\t" + host.address)
        #print("\t%015s %025s" & ("auto_numa_status",host.auto_numa_status))
        #print("\t%015s %025s" & ("certificate",host.certificate))
        #print("\t%015s %025s" & ("comment",host.comment))
        #print("\t%015s %025s" & ("description",host.description))
        #print("\t%015s %025s" & ("device_passthrough",host.device_passthrough))
        #print("\t%015s %025s" & ("display",host.display))
        #print("\t%015s %025s" & ("external_status",host.external_status))
        #print("\t%015s %025s" & ("hardware_information",host.hardware_information))
        #print("\t%015s %025s" & ("id",host.id))
        #print("\t%015s %025s" & ("iscsi",host.iscsi))
        #print("\t%015s %025s" & ("kdump_status",host.kdump_status))
        #print("\t%015s %025s" & ("ksm",host.ksm))
        #print("\t%015s %025s" & ("libvirt_version",host.libvirt_version))
        #print("\t%015s %025s" & ("max_scheduling_memory",host.max_scheduling_memory))
        #print("\t%015s %025s" & ("network_operation_in_progress",host.network_operation_in_progress))
        #print("\t%015s %025s" & ("numa_supported",host.numa_supported))
        #print("\t%015s %025s" & ("os",host.os))
        #print("\t%015s %025s" & ("override_iptables",host.override_iptables))
        #print("\t%015s %025s" & ("port",host.port))
        #print("\t%015s %025s" & ("protocol",host.protocol))
        #print("\t%015s %025s" & ("reinstallation_required",host.reinstallation_required))
            #??? print("\t%015s %025s" % ("root_password",host.root_password))
        #print("\t%015s %025s" & ("se_linux",host.se_linux))
        #print("\t%015s %025s" & ("ssh",host.ssh))
        #print("\t%015s %025s" & ("summary",host.summary))
        #print("\t%015s %025s" & ("transparent_huge_pages",host.transparent_huge_pages))
        #print("\t%015s %025s" & ("type",host.type))
        #print("\t%015s %025s" & ("update_available",host.update_available))
        #print("\t%015s %025s" & ("vgpu_placement",host.vgpu_placement))
            print(getDedicatedStatistics(host.statistics,  statsMode.hostLoad))



def RHV_getStats(query, obj_name):
    global connection
    if query == ObjType.HOST:
        # Find the host:
        hosts_service = connection.system_service().hosts_service()
        host = hosts_service.list(search='name=' + obj_name + '*')[0]
        print(host.name)
        getStatistics(host.statistics)
    elif query == ObjType.VM:
        vm_srv=VM_get(obj_name)
        if vm_srv == None:
            return
        vm=vm_srv.get()
        print(vm.name)
        #getStatistics(vm.statistics) #, statsMode.FULL, printMode.get )
        getStatistics(vm.statistics) #, statsMode.cpu)



#############################################################################
####                                                                     ####
####     Virtual Machine functions                                       ####
####                                                                     ####
#############################################################################



def VM_get(VM):
    vms_service = getVMSService()
    vms = vms_service.list(search='name='+VM, max=1)

    # Retrieve the representation of the virtual machine:
    if len(vms) == 0:
        print("NO vm found!")
        return None

    vm=vms[0]
    return vms_service.vm_service(vm.id)



def VM_getInfo(VM):
    vms_service = getVMSService()
    vms = vms_service.list(search='name='+VM, max=1)

    # Retrieve the representation of the virtual machine:
    if len(vms) == 0:
        print("NO vm found!")
        return ""

    vm=vms[0]

    return vm.status



def VM_printInfo(VM):
    vms_service = getVMSService()
    vms = vms_service.list(search='name='+VM, max=1)

    # Retrieve the representation of the virtual machine:
    if len(vms) == 0:
        print("NO vm found!")
        return

    vm=vms[0]

    out = vm.name + " - " + vm.status.name
    print(out)
    #print(getDedicatedStatistics(vm.statistics,  statsMode.migration))



def VM_Start(VM):
    cmd_run=1

    # Retrieve the representation of the virtual machine:
    vm_srv=VM_get(VM)
    vm=vm_srv.get()

    print(vm.name + " - " + vm.status.name)
    print(" \u25B2\u25B2\u25B2 ")
    status = vm.status
    while status != types.VmStatus.UP:
        if cmd_run == 1:
            vm_srv.start()
            cmd_run = 0
        vm=vm_srv.get()
        if vm.status != status :
            status = vm.status
            if status != types.VmStatus.UP:
                print("  >>    " + vm.status.name)
                time.sleep(5)
    print(vm.name + " - " + vm.status.name)



def VM_MigrateTo(VM, HOST):
    cmd_run=1

    # Retrieve the representation of the virtual machine:
    vm_srv=VM_get(VM)
    vm=vm_srv.get()
    original_host=connection.follow_link(vm.host)
    original_host_name=original_host.name.split(".")[0]

    # check fttibility
    if vm.status != types.VmStatus.UP:
        print("VM is in impossibility status for migration: " + vm.status.name)
        return

    # recover host obj
    host_obj=""
    host_service = connection.system_service().hosts_service()
    hosts = host_service.list()
    for host in hosts:
        if host.name.split(".")[0] == HOST and host.status != types.HostStatus.MAINTENANCE:
            host_obj = host
            host_obj_name=host_obj.name.split(".")[0]
    
    if host_obj == "":
        cluster=connection.follow_link(vm.cluster)
        print("Missing valid host name!  Available HOST are:")
        #print(original_host_name)
        for host in hosts:
            cluster_host=connection.follow_link(host.cluster)
            if cluster_host.name == cluster.name and original_host_name != host.name.split(".")[0] :
                #print("\t" + host.name )
                print("\t" + host.name.split(".")[0] + "\t (" + host.status.name + ")")
        return
    elif original_host_name == host_obj_name:
        print("Already in " + original_host_name)
        return

    print("- %-28s %s =>" % (vm.name, original_host_name ))
 
    #print(" \u25B6\u25B6\u25B6 ")
    vm_srv.migrate(host=host_obj)
    time.sleep(1)

    waitstr=[ "|", "/", "-", "\\"]
    i=0
    while True:
        vm = vm_srv.get()
        if vm.status != types.VmStatus.MIGRATING:
            break
        percMig=getDedicatedStatistics(vm.statistics, statsMode.migration)
        print("  \u25B6\u25B6\u25B6\u25B6    " + host_obj_name , end='')
        sys.stdout.write("\r "  + "(" + str(percMig) + "%) " + vm.status.name + "  " + waitstr[i % len(waitstr)] )
        sys.stdout.flush()
        time.sleep(2)
        i=i+1

    dest_host=connection.follow_link(vm.host)
    dest_host_name=dest_host.name.split(".")[0]
    #print("\n" + vm.name + " -  => " + dest_host.name)
    print("")
    if original_host_name == dest_host_name:
        print(">>> ERROR: some problem during migration!!!")
    else:
        print(" %-29s %s <=" % (vm.name, dest_host_name ))
        print("Migration complete")



def VM_Shutdown(VM):
    cmd_run=1

    # Retrieve the representation of the virtual machine:
    vm_srv=VM_get(VM)
    vm=vm_srv.get()

    print(vm.name + " - " + vm.status.name)
    print(" \u25BC\u25BC\u25BC ")
    status = vm.status
    while status != types.VmStatus.DOWN:
        if cmd_run == 1:
            vm_srv.shutdown()
            cmd_run = 0
        vm=vm_srv.get()
        if vm.status != status :
            status = vm.status
            if status != types.VmStatus.DOWN:
                print("  >>    " + vm.status.name)
                time.sleep(5)
    print(vm.name + " - " + vm.status.name)



def VM_Restart(VM):
    VM_Shutdown(VM)
    VM_Start(VM)



def VM_List(VM, target_host="*", mode=infoMode.FULL):
    vms_service = getVMSService()
    vms = vms_service.list(search='name='+VM)

    # Retrieve the representation of the virtual machine:
    if mode != infoMode.MINIMAL:
        if target_host == "*":
            print("\n%032s\t%015s\t%020s\t%12s\t%06s" % ("VM NAME", "CLUSTER", "HOST", "STATUS", "MEMORY"))
        else:
            print("\n%032s\t%12s\t%06s" % ("VM NAME", "STATUS", "MEMORY"))
    cnted=0
    totmem=0
    for vm in vms:
        cluster=connection.follow_link(vm.cluster)
        host_name="-----"
        #if vm.status == types.VmStatus.UP:
        if vm.status != types.VmStatus.DOWN:
            host_name=(connection.follow_link(vm.host).name.split("."))[0]
        if target_host == "*" or host_name == target_host:
            cnted=cnted+1
            if mode == infoMode.MINIMAL:
                print("%032s" % (vm.name))
            else:
                if target_host == "*":
                    print("%032s\t%015s\t%020s\t%12s\t%06s" % (vm.name,cluster.name, host_name, vm.status.name, (vm.memory / 1024/1024/1024)))
                else:
                    #print("%032s\t%015s\t%12s\t%06s" % (vm.name,cluster.name, vm.status.name, (vm.memory / 1024/1024/1024)))
                    print("%032s\t%12s\t%06s" % (vm.name, vm.status.name, (vm.memory / 1024/1024/1024)))
                totmem+=(vm.memory / 1024/1024/1024)
    if target_host != "*":
        #print("%032s\t%015s\t%12s\t%06s" % ("","","TOT MEM", (totmem)))
        print("%032s\t%12s\t%06s" % ("","TOT MEM", (totmem)))
    return cnted



def VM_DiskList(VM):
    vms_service = getVMSService()
    vms = vms_service.list(search='name='+VM)
    full_disk_size = 0

    # Retrieve the representation of the virtual machine:
    print("\n%032s %015s %05s %036s - % 5s GB  %025s\t%05s" % ("VM NAME", "CLUSTER", "STATUS", "Disk name", "Size", "Storage", "QOS"))
    for vm in vms:
        #print("%032s %06s" % (vm.name, vm.status.name))
        disk_attachments = vms_service.vm_service(vm.id).disk_attachments_service().list()
        cluster=connection.follow_link(vm.cluster)
        disk_size = 0
        vm_name = "NOT DISK"
        for disk_attachment in disk_attachments:
            if disk_size == 0:
                vm_name = vm.name
                vm_status = vm.status.name
            else:
                vm_name = ""
                vm_status = ""
            disk = connection.follow_link(disk_attachment.disk)
            disk_size += disk.provisioned_size
            full_disk_size += disk.provisioned_size
            if disk.disk_profile == None:
                storage="Undefined"
                profile_str="Undefined"
            else:
                disk_profile =  connection.follow_link(disk.disk_profile)
                sd = connection.follow_link(disk_profile.storage_domain)
                storage = sd.name
                if disk_profile.name == sd.name:
                    profile_str= "unlimited"
                else:
                    profile_str=disk_profile.name

            print("%032s %015s %05s %037s - % 5d GB  %025s\t%05s" % (
                    vm_name,
                    cluster.name,
                    vm_status,
                    disk.name,
                    (disk.provisioned_size / 1073741824),
                    storage,
                    profile_str
            ))
        if vm_name == "":
            print("%032s %05s %037s - % 5d GB  %025s\t%05s" % (
                    "",
                    "",
                    "TOT",
                    (disk_size / 1073741824),
                    "",
                    ""
            ))
    if len(vms) > 1:
        print("%032s %05s %037s - % 5d GB  %025s\t%05s" % (
                "",
                "",
                "FOR " + str(len(vms)) + " VMs",
                (full_disk_size / 1073741824),
                "",
                ""
        ))



def VM_DiskMove(VM, dest_domain):
    vms_service = getVMSService()
    vms = vms_service.list(search='name='+VM)

    # Retrieve the representation of the virtual machine:
    print("\n%032s %015s %015s %05s %036s - % 5s GB  %025s\t%05s" % ("VM NAME", "ID", "CLUSTER", "STATUS", "Disk name", "Size", "Storage", "QOS"))
    for vm in vms:
        #print("%032s %06s" % (vm.name, vm.status.name))
        disk_attachments = vms_service.vm_service(vm.id).disk_attachments_service().list()
        cluster=connection.follow_link(vm.cluster)
        disk_size = 0
        vm_name = "NOT DISK"
        for disk_attachment in disk_attachments:
            if disk_size == 0:
                vm_name = vm.name
                vm_status = vm.status.name
            else:
                vm_name = ""
                vm_status = ""
            disk = connection.follow_link(disk_attachment.disk)
            disk_size += disk.provisioned_size
            if disk.disk_profile == None:
                storage="Undefined"
                profile_str="Undefined"
            else:
                disk_profile =  connection.follow_link(disk.disk_profile)
                sd = connection.follow_link(disk_profile.storage_domain)
                storage = sd.name
                if disk_profile.name == sd.name:
                    profile_str= "unlimited"
                else:
                    profile_str=disk_profile.name

            print("%032s %015s %015s %05s %037s - % 5d GB  %025s\t%05s" % (
                    vm_name,
                    disk.id,
                    cluster.name,
                    vm_status,
                    disk.name,
                    (disk.provisioned_size / 1073741824),
                    storage,
                    profile_str
            ))
        if vm_name == "":
            print("%032s %05s %037s - % 5d GB  %025s\t%05s" % (
                    "",
                    "",
                    "TOT",
                    (disk_size / 1073741824),
                    "",
                    ""
            ))



        sds_service = getSDSService()
        sd = sds_service.list(search='name=' + dest_domain)[0]
        DSRVS=connection.system_service().disks_service()
        DSRV=DSRVS.disk_service(disk.id)
        #DSRV.move(self, async_=None, disk_profile=None, filter=None, quota=None, storage_domain=None, headers=None, query=None, wait=True, **kwargs)
        ### DSRV.move(async_=None, disk_profile=None, filter=None, quota=None, storage_domain=sd)



def VM_FullInfo(str, mode, prn=printMode.formatted) :
    # list of all VMs
    # section:

    vms_service = connection.system_service().vms_service()
    virtual_machines = vms_service.list(search='name='+str)

    if len(virtual_machines) > 0:
        for virtual_machine in virtual_machines:
            vm_service = vms_service.vm_service(virtual_machine.id)

            if prn == printMode.formatted:
                print("\n==================================================")
                print("%-15s %s" % ("Name VM:",     virtual_machine.name))
                if mode == infoMode.FULL:
                    print("%-15s %s" % ("@@@@ VM:",     DATAS.getStringVersion(virtual_machine.guest_operating_system,virtual_machine.os)))
                    print("%-15s # %d / %d / %d (C/S/T)" % ("vCPU:", virtual_machine.cpu.topology.cores, virtual_machine.cpu.topology.sockets, virtual_machine.cpu.topology.threads))
                    print("%-15s %s GB" % ("RAM:", (virtual_machine.memory / 1024 /1024/1024)))

            cluster=connection.follow_link(virtual_machine.cluster)
            if prn == printMode.formatted:
                print("%-15s %s" % ("Cluster:",     cluster.name))

            host_name="n/a"
            if virtual_machine.status == types.VmStatus.UP:
                host_name=connection.follow_link(virtual_machine.host).name
            if prn == printMode.formatted:
                print("%-15s %s" % ("Host:",        host_name.split(".")[0]))

            if mode == infoMode.FULL:
                disk_attachments = vm_service.disk_attachments_service().list()
                disk_size = 0
                for disk_attachment in disk_attachments:
                    disk = connection.follow_link(disk_attachment.disk)
                    disk_size += disk.provisioned_size
                    if disk.disk_profile:
                        disk_profile =  connection.follow_link(disk.disk_profile)

                        #if prn == printMode.formatted:
                            #print(" %-35s: % 5d GB" % ( disk.name, (disk_size / 1073741824)))
                            #print(disk.id)
                            #print(disk.disk_profile)
                            #print(disk.disk_profile.id)
                            #print(disk.disk_profile.qos)
                            #print(disk.disk_profile.name)
                            #print(disk.disk_profile.storage_domain)
                            #print(disk.disk_profile.permissions)
                        disk_profile = connection.follow_link(disk.disk_profile)
                        sd = connection.follow_link(disk_profile.storage_domain)
                        storage = sd.name
                        if disk_profile.name == sd.name:
                            profile_str= "unlimited"
                        else:
                            profile_str=disk_profile.name
                        if prn == printMode.formatted:
                            print(" %-35s: % 5d GB  %025s\t%05s" % (
                                    disk.name,
                                    (disk.provisioned_size / 1073741824),
                                    storage,
                                    profile_str
                            ))
                if prn == printMode.formatted:
                    print("%032s %05s %037s - % 5d GB  %025s\t%05s" % (
                            "",
                            "",
                            "TOT",
                            (disk_size / 1073741824),
                            "",
                            ""
                    ))



#############################################################################
####                                                                     ####
####     Storage - Data Domain functions                                 ####
####                                                                     ####
#############################################################################



def SD_activate(attached_sd_service):
    cmd_run=1
    sd = attached_sd_service.get()
    print(sd.name + " - " + sd.status.name)
    print(" \u25B2\u25B2\u25B2 ")
    status = sd.status
    while status != types.StorageDomainStatus.ACTIVE:
        if cmd_run == 1:
            cmd_run = 0
            attached_sd_service.activate()
        sd = attached_sd_service.get()
        if sd.status != status:
            status = sd.status
            if status != types.StorageDomainStatus.ACTIVE:
                print("  >>    " + status.name)
        time.sleep(2)
    print(sd.name + " - " + sd.status.name)



def SD_deactivate(attached_sd_service):
    cmd_run=1
    sd = attached_sd_service.get()
    print(sd.name + " - " + sd.status.name)
    print(" \u25BC\u25BC\u25BC ")
    status = sd.status
    while status != types.StorageDomainStatus.MAINTENANCE:
        if cmd_run == 1:
            cmd_run = 0
            attached_sd_service.deactivate()
        sd = attached_sd_service.get()
        if sd.status != status:
            status = sd.status
            if status != types.StorageDomainStatus.MAINTENANCE:
                print("  >>    " + status.name)
        time.sleep(2)
    print(sd.name + " - " + sd.status.name)



def SD_check_import(storage_domain_service, vm_name):
    vms_service = storage_domain_service.vms_service()
    exported_vms = vms_service.list()
    #print (exported_vms)
    found_it=0
    for vm in exported_vms:
        #print("    VM on export domain: %s (%s) create on %s" % (vm.name, vm.status, vm.creation_time))
        if vm.name.lower() == vm_name.lower():
            found_it = 1

    if found_it == 1:
        print("    VM on export domain: %s (%s) create on %s" % (vm.name, vm.status, vm.creation_time))
        #for property, value in vars(vm.initialization).items():
        #    print(property, ":", value)
    else:
        print("WARNING: NO VM FOUND!!!!")




#############################################################################
####                                                                     ####
####     Support and cumulative general purpose functions                ####
####                                                                     ####
#############################################################################



def VM_export(export_domain, vm_name, dc_name="default"):
    print("==> Export start at %s" % (time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())))
    sds_service = getSDSService()
    sd = sds_service.list(search='name=' + export_domain)[0]

    dc_service = getDCService(dc_name)
    attached_sds_service = dc_service.storage_domains_service()
    attached_sd_service = attached_sds_service.storage_domain_service(sd.id)
    sd_service = sds_service.storage_domain_service(sd.id)
    
    SD_activate(attached_sd_service)

    sd = attached_sd_service.get()
    print(attached_sds_service)

    print("Attached data storage domain '%s' to data center '%s' (Status: %s)." % (sd.name, "Default", sd.status.name))

    ### to make a function
#    print "\n"+strftime("%a, %d %b %Y %H:%M:%S", gmtime())+" : Export of the virtual machine"

    vm_srv = VM_get(vm_name)
    curr_VM_status = vm_srv.get().status

    if curr_VM_status != types.VmStatus.DOWN:
        VM_Shutdown(vm_name)

    vm_srv.export_to_export_domain(_async=None, exclusive=True, discard_snapshots=True, storage_domain=sd)

    vm = vm_srv.get()
    basestr = vm.status.name
    waitstr=[ "|", "/", "-", "\\"]
    i=0
    while True:
        vm = vm_srv.get()
        print(".", end='')
        sys.stdout.write("\r" + basestr + " " + waitstr[i % len(waitstr)])
        sys.stdout.flush()
        time.sleep(2)
        i=i+1
        if vm.status == types.VmStatus.DOWN:
            break
    sys.stdout.write("\r" + basestr + " \u2055\n")
    sys.stdout.flush()
    time.sleep(1)

    SD_check_import(sd_service, vm_name)

    if vm.status != curr_VM_status:
        VM_Start(vm_name)

    SD_deactivate(attached_sd_service)

    sd = attached_sd_service.get()
    print("Attached data storage domain '%s' to data center '%s' (Status: %s)." % (sd.name, "Default", sd.status.name))
    print("==> Export end at %s" % (time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())))



def getDedicatedStatistics(ref_statistics, statsWhat=statsMode.migration):
# migration.progress        mig             VM
    # Follow the link to the statistics and print their names and values:
    stats = connection.follow_link(ref_statistics)
    match statsWhat:
        case statsMode.migration:
            matchingKey="migration.progress"
        case statsMode.hostLoad:
            # memory.used                    HOST    VM
            # cpu.load.avg.5m                HOST
            matchingKey="memory.used cpu.load.avg.5m"
        case _:
            return ""

    retVal=""
#    for stat in stats:
#        if stat.name == matchingKey:
#            #return(stat.values[0].datum)
#            retVal=stat.values[0].datum

    #print(matchingKey)
    for ref in matchingKey.split(" "):
        #print("- " + ref)
        for stat in stats:
            if stat.name == ref:
                #print("found it => " + str(stat.values[0].datum))
                if retVal == "":
                    retVal=str(stat.values[0].datum)
                else:
                    retVal=retVal + " " + str(stat.values[0].datum)

    return(retVal)



def getStatistics(ref_statistics, statsWhat=statsMode.full, statsHow=printMode.formatted):
# Collectect statistics
#
# memory.total                   HOST
# memory.used                    HOST    VM
# memory.free                    HOST    VM
# memory.installed                       VM
# memory.shared                  HOST
# memory.buffers                 HOST
# memory.buffered                        VM
# memory.cached                  HOST    VM
# memory.usage.history                   VM
# swap.total                     HOST
# swap.free                      HOST
# swap.used                      HOST
# swap.cached                    HOST
# ksm.cpu.current                HOST
# cpu.current.user               HOST
# cpu.current.system             HOST
# cpu.current.idle               HOST
# cpu.current.total                      VM
# cpu.current.hypervisor                 VM
# cpu.current.guest                      VM
# cpu.load.avg.5m                HOST
# cpu.usage.history                      VM
# disks.usage                            VM
# network.current.total                  VM
# network.usage.history                  VM
# migration.progress                     VM
# hugepages.1048576.free         HOST
# hugepages.2048.free            HOST
# boot.time                      HOST
# elapsed.time                           VM

    # Follow the link to the statistics and print their names and values:
    stats = connection.follow_link(ref_statistics)
    for stat in stats:
        hwClass=stat.name.split(".")[0]
        #print("---" + hwClass)
        if not ( hwClass in statsMode.__members__ ):
            print(" -- " + stat.name + " - " + stat.description + " " + str(stat.values[0].datum) + "( " +  str(len(stat.values)) + " )")
        elif statsMode[hwClass].value & statsWhat.value :
            #print("  ### Vero")
            #print("      " + statsMode[hwClass].name)
            #print("      " + str(statsMode[hwClass].value))
            #print("      " + str(statsWhat.value))
            if statsHow == printMode.formatted:
                print("%-25s (%02d)" % (stat.description, len(stat.values)))
            ##print("\t" + stat.id)
            ##print(stat.kind)
            #if statsHow == printMode.formatted:
            #    print(stat.name)
            #print(stat.type)
            unitMesured=stat.unit
            idx=0
            if stat.name == "disks.usage":
                if statsHow == printMode.formatted:
                    print("DISCO")
                    print(stat.values[0].detail)
            while idx < len(stat.values):
                if statsHow == printMode.formatted:
                    print("    ", end='')
                match unitMesured:
                    case types.StatisticUnit.BYTES:
                        MB=stat.values[idx].datum / 1024 / 1024
                        if statsHow == printMode.formatted:
                            print("%05.1f MB" % (MB), end='')
                    case _:
                        if statsHow == printMode.formatted:
                            print(stat.values[idx].datum, end='')
                idx=idx+1
                if idx > 28:
                    break
            if statsHow == printMode.formatted:
                print("")



#############################################################################
####                                                                     ####
####     Procedure in testing phase                                      ####
####                                                                     ####
#############################################################################



def TEST3():
    # Find the service that manages the collection of events:
    global connection
    events_service = connection.system_service().events_service()
    
    page_number = 1
    events = events_service.list(search='page %s' % page_number)
    while events:
        for event in events:
            print(
                "%s %s CODE %s - %s" % (
                    event.time,
                    event.severity,
                    event.code,
                    event.description,
                )
            )
        page_number = page_number + 1
        events = events_service.list(search='page %s' % page_number)



####------TESST#



def TEST3():
    # Find the service that manages the collection of events:
    global connection
    events_service = connection.system_service().events_service()
    
    page_number = 1
    events = events_service.list(search='page %s' % page_number)
    while events:
        for event in events:
            print(
                "%s %s CODE %s - %s" % (
                    event.time,
                    event.severity,
                    event.code,
                    event.description,
                )
            )
        page_number = page_number + 1
        events = events_service.list(search='page %s' % page_number)


'''

# Locate the virtual machines service and use it to find the virtual
# machine:
vms_service = connection.system_service().vms_service()
vm = vms_service.list(search='name=vm1')[0]

# Locate the service that manages the network interface cards of the
# virtual machine:
nics_service = vms_service.vm_service(vm.id).nics_service()

# Locate the vnic profiles service and use it to find the ovirmgmt
# network's profile id:
profiles_service = connection.system_service().vnic_profiles_service()
profile_id = None
for profile in profiles_service.list():
    if profile.name == 'ovirtmgmt':
        profile_id = profile.id
        break

# Use the "add" method of the network interface cards service to add the
# new network interface card:

nic = nics_service.add(
    types.Nic(
        name='nic1',
        interface=types.NicInterface.VIRTIO,
        vnic_profile=types.VnicProfile(id=profile_id),
    ),
)

print("Network interface '%s' added to '%s'." % (nic.name, vm.name))
'''
