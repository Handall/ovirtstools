
import configparser

config = configparser.ConfigParser()

def CFG_Load(filename):
    # Reading Data
    config.read(filename)



def CFG_GetDefault():
    global config

    return config.get("DEFAULT", "default_manager")



# related by RHEV
def CFG_GetManagers():
    global config
    return config.sections()



def CFG_GetConfiguration(section):
    global config
    print( config[section] )
    exit()



def CFG_Get(section, key):
    global config
    return config.get(section, key)

#print ( config.get("DEFAULT", "default_manager"))
#print(config.sections())
